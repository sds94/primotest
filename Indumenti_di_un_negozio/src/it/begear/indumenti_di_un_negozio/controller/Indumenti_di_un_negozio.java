package it.begear.indumenti_di_un_negozio.controller;

import it.begear.indumenti_di_un_negozio.dao.DAOIndumenti_di_un_negozio;
import it.begear.indumenti_di_un_negozio.dao.DAOIndumenti_di_un_negozioImpl;
import it.begear.indumenti_di_un_negozio.exc.MiaException;
import it.begear.indumenti_di_un_negozio.exc.MiaException2;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_donna;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_uomo;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_venduto;
import it.begear.indumenti_di_un_negozio.repository.Array;
import it.begear.indumenti_di_un_negozio.service.Inserimento_donna;
import it.begear.indumenti_di_un_negozio.service.Inserimento_uomo;
import it.begear.indumenti_di_un_negozio.util.ScannerManager;
import it.begear.indumenti_di_un_negozio.view.Cli;


public class Indumenti_di_un_negozio {

	public static void main(String[] args) throws MiaException, MiaException2 {
	int scelta;
	
	Cli.benvenuto();
	do {
		Cli.menu();
		scelta=ScannerManager.scrivere_numero();
		switch (scelta) {
		case 1:{
			DAOIndumenti_di_un_negozio daoIndumenti_di_un_negozio= DAOIndumenti_di_un_negozioImpl.getInstance();
			Abbigliamento_uomo  vestito = new Inserimento_uomo().inserimento_uomo();
			Array.save(vestito);
			break;
		}
		case 2:{
			DAOIndumenti_di_un_negozio daoIndumenti_di_un_negozio= DAOIndumenti_di_un_negozioImpl.getInstance();
			Abbigliamento_donna  vestito = new Inserimento_donna().inserimento_donna();
			Array.save(vestito);
			break;
		}
		
		case 3:{
			
			Array.elimina_elemento();
			break;
		}
		case 4:{
			Array.stampa();
			break;
		}
		case 5:{
			Array.ricerca();
			break;
		}
		case 6:{
			Array.venduto();
			break;
		}
		case 7:{
			Array.scontato();
			break;
		}
		case 8:{
			Cli.arrivederci();
			break;
		}
		default:{
			break;
		}
		}
	}while(scelta!=8);

}
 }