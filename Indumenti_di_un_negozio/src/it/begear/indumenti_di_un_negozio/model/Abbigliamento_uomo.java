package it.begear.indumenti_di_un_negozio.model;

import it.begear.indumenti_di_un_negozio.util.ScannerManager;

public class Abbigliamento_uomo extends Abbigliamento {
private int id_uomo;
private boolean da_cerimonia;
private boolean sportivo;
public Abbigliamento_uomo() {
	super();
}
public Abbigliamento_uomo(int id_uomo,boolean da_cerimonia,boolean sportivo,
	String materiale,int totale,int venduti,int prezzo,String nome) {
	super(materiale, totale, venduti, prezzo,nome);
	this.id_uomo=id_uomo;
	this.da_cerimonia=da_cerimonia;
	this.sportivo=sportivo;
	
}

	public int getId_uomo() {
	return id_uomo;
}
public void setId_uomo(int id_uomo) {
	this.id_uomo = id_uomo;
}
	public boolean isDa_cerimonia() {
	return da_cerimonia;
}
public void setDa_cerimonia(boolean da_cerimonia) {
	this.da_cerimonia = da_cerimonia;
}
public boolean isSportivo() {
	return sportivo;
}
public void setSportivo(boolean sportivo) {
	this.sportivo = sportivo;
}
@Override
public void venduto() {
	// TODO Auto-generated method stub
	int totale=super.getTotale();
	int venduti=super.getVenduti();
	System.out.println("venduto "+super.getNome());
    totale--;
    venduti++;
    super.setTotale(totale);
    super.setVenduti(venduti);
}
@Override
public void scontato() {
	// TODO Auto-generated method stub
	int prezzo=super.getPrezzo();
	System.out.println("inserisci il valore di sconto");
	int sconto=ScannerManager.scrivere_numero();
	System.out.println("� scontato del "+sconto+"%");
	prezzo=prezzo-((prezzo*sconto)/100);
	super.setPrezzo(prezzo);
}
@Override
public String toString() {
	return "Abbigliamento_uomo [id_uomo=" + id_uomo + ", da_cerimonia=" + da_cerimonia + ", sportivo=" + sportivo
			+ ", Materiale=" + getMateriale() + ", Totale=" + getTotale() + ", Venduti=" + getVenduti()
			+ ", Prezzo=" + getPrezzo() + ", Nome=" + getNome() + "]";
}



}
