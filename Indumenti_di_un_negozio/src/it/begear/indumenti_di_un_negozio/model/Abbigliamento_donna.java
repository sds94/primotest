package it.begear.indumenti_di_un_negozio.model;

import it.begear.indumenti_di_un_negozio.util.ScannerManager;

public class Abbigliamento_donna extends Abbigliamento {
	private int id_donna;
	private boolean corto;
	private boolean attraente;
	public Abbigliamento_donna() {
		super();
	}
	
	public Abbigliamento_donna(int id_donna,boolean corto,boolean attraente,
		String materiale,int totale,int venduti,int prezzo,String nome) {
		super(materiale, totale, venduti, prezzo,nome);
		this.corto=corto;
		this.attraente=attraente;
		this.id_donna=id_donna;
		
	}
     
	
	

	public int getId_donna() {
		return id_donna;
	}

	public void setId_donna(int id_donna) {
		this.id_donna = id_donna;
	}

	public boolean isCorto() {
		return corto;
	}

	public void setCorto(boolean corto) {
		this.corto = corto;
	}

	public boolean isAttraente() {
		return attraente;
	}

	public void setAttraente(boolean attraente) {
		this.attraente = attraente;
	}

	@Override
	public void venduto() {
		// TODO Auto-generated method stub
		int totale=super.getTotale();
		int venduti=super.getVenduti();
		System.out.println("venduto "+super.getNome());
		totale--;
	    venduti++;
	    super.setTotale(totale);
	    super.setVenduti(venduti);
	}
	@Override
	public void scontato() {
		// TODO Auto-generated method stub
		int prezzo=super.getPrezzo();
		System.out.println("inserisci il valore di sconto");
		int sconto=ScannerManager.scrivere_numero();
		System.out.println("� scontato del "+sconto+"%");
		prezzo=prezzo-((prezzo*sconto)/100);
		super.setPrezzo(prezzo);
	}

	@Override
	public String toString() {
		return "Abbigliamento_donna [id_donna=" + id_donna + ", corto=" + corto + ", attraente=" + attraente
				+ ", Materiale=" + getMateriale() + ", Totale=" + getTotale() + ", Venduti="
				+ getVenduti() + ", Prezzo=" + getPrezzo() + ", Nome=" + getNome() + "]";
	}


}
