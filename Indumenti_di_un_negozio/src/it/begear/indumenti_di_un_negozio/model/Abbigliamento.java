package it.begear.indumenti_di_un_negozio.model;

import it.begear.indumenti_di_un_negozio.util.ScannerManager;

public abstract class Abbigliamento implements Abbigliamento_venduto{
private String nome;
private String materiale;
private int totale;
private int venduti;
private int prezzo;
public Abbigliamento() {
	
}
public Abbigliamento(String materiale,int totale,int venduti,int prezzo,String nome) {
	this.materiale=materiale;
	this.totale=totale;
	this.venduti=venduti;
	this.setPrezzo(prezzo);
	this.nome=nome;
}
public String getMateriale() {
	return materiale;
}
public void setMateriale(String materiale) {
	this.materiale = materiale;
}
public int getTotale() {
	return totale;
}
public void setTotale(int totale) {
	this.totale = totale;
}
public int getVenduti() {
	return venduti;
}
public void setVenduti(int venduti) {
	this.venduti = venduti;}
public int getPrezzo() {
	return prezzo;
}
public void setPrezzo(int prezzo) {
	this.prezzo = prezzo;
}

public String getNome() {
	return nome;
}
public void setNome(String nome) {
	this.nome = nome;
}
@Override
public String toString() {
	return "Abbigliamento [nome=" + nome + ",materiale=" + materiale + ", totale=" + totale + ", venduti=" + venduti + ", prezzo=" + prezzo + "]";
}


}
