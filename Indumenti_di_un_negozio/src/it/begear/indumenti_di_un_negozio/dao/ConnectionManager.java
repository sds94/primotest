package it.begear.indumenti_di_un_negozio.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

	private ConnectionManager() {
	}

	static Connection getConnection() throws SQLException { 

		String url = "jdbc:mysql://localhost:3306/primodbc?useSSL=false&serverTimezone=UTC";
		String user = "root";
		String password = "Salvatore1994";

		return DriverManager.getConnection(url, user, password);

	}

}
