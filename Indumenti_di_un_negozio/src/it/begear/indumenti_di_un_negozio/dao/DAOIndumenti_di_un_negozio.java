package it.begear.indumenti_di_un_negozio.dao;

import java.util.List;

import it.begear.indumenti_di_un_negozio.model.Abbigliamento;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_donna;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_uomo;


public interface DAOIndumenti_di_un_negozio{

	void createIndumentouomo(Abbigliamento_uomo vestito);
	 
	Abbigliamento_uomo readAbbigliamentouomo(int totale);
	
	void updateAbbigliamentouomo(Abbigliamento_uomo vestito);
	
	void deleteAbbigliamentouomo(Abbigliamento_uomo vestito);
	
	void createIndumentodonna(Abbigliamento_donna vestito);
	 
	Abbigliamento_donna readAbbigliamentodonna(int totale);
	
	void updateAbbigliamentodonna(Abbigliamento_donna vestito);
	
	void deleteAbbigliamentodonna(Abbigliamento_donna vestito);
	
	
	
	
}