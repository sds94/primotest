package it.begear.indumenti_di_un_negozio.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import it.begear.indumenti_di_un_negozio.model.Abbigliamento;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_donna;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_uomo;



public class DAOIndumenti_di_un_negozioImpl implements DAOIndumenti_di_un_negozio{
	
	private static DAOIndumenti_di_un_negozioImpl instance= null;
	
	private DAOIndumenti_di_un_negozioImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public static DAOIndumenti_di_un_negozioImpl getInstance() {
		if(instance==null) {
			instance=new DAOIndumenti_di_un_negozioImpl();
		}
		return instance;
	}
	
	

	

	

	@Override
	public void createIndumentouomo(Abbigliamento_uomo vestito) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO vestito (totale, nome, materiale) VALUES (?, ?, ?)";

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			stm.setInt(1, vestito.getTotale());
			stm.setString(3, vestito.getNome());
			stm.setString(2, vestito.getMateriale());
			

			stm.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}



		
	}
	@Override
	public void createIndumentodonna(Abbigliamento_donna vestito) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO vestito (totale, nome, materiale) VALUES (?, ?, ?)";

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			stm.setInt(1, vestito.getTotale());
			stm.setString(3, vestito.getNome());
			stm.setString(2, vestito.getMateriale());
			

			stm.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}



		
	}

	@Override
	public Abbigliamento_uomo readAbbigliamentouomo(int totale) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM vestito WHERE totale=?";
		Abbigliamento_uomo vestito= null;

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			vestito = new Abbigliamento_uomo();
			stm.setInt(1, totale);
			ResultSet result = stm.executeQuery();

			while (result.next()) {

				vestito.setTotale(result.getInt("totale"));
				vestito.setNome(result.getString("nome"));
				vestito.setMateriale(result.getString("materiale"));
			

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return vestito;
		
	}
	@Override
	public Abbigliamento_donna readAbbigliamentodonna(int totale) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM vestito WHERE totale=?";
		Abbigliamento_donna vestito= null;

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			vestito = new Abbigliamento_donna();
			stm.setInt(1, totale);
			ResultSet result = stm.executeQuery();

			while (result.next()) {

				vestito.setTotale(result.getInt("totale"));
				vestito.setNome(result.getString("nome"));
				vestito.setMateriale(result.getString("materiale"));
			

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return vestito;
		
	}

	@Override
	public void updateAbbigliamentouomo(Abbigliamento_uomo vestito) {
		// TODO Auto-generated method stub
		String sql = "UPDATE vestito SET nome=?, materiale=? WHERE totale=?";

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			stm.setString(1, vestito.getNome());
			stm.setString(2, vestito.getMateriale());
			stm.setInt(3, vestito.getTotale());

			stm.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void updateAbbigliamentodonna(Abbigliamento_donna vestito) {
		// TODO Auto-generated method stub
		String sql = "UPDATE vestito SET nome=?, materiale=? WHERE totale=?";

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			stm.setString(1, vestito.getNome());
			stm.setString(2, vestito.getMateriale());
			stm.setInt(3, vestito.getTotale());

			stm.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void deleteAbbigliamentouomo(Abbigliamento_uomo vestito) {
		// TODO Auto-generated method stub
		String sql= "DELETE FROM vestito where totale=?";
		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);){
			
			stm.setInt(1, vestito.getTotale());
			stm.execute();
			
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public void deleteAbbigliamentodonna(Abbigliamento_donna vestito) {
			// TODO Auto-generated method stub
			String sql= "DELETE FROM vestito where totale=?";
			try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);){
				
				stm.setInt(1, vestito.getTotale());
				stm.execute();
				
				}catch (SQLException e) {
					e.printStackTrace();
				}
			
	}


	


	

}