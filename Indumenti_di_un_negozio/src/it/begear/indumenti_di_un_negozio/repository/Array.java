package it.begear.indumenti_di_un_negozio.repository;

import java.util.ArrayList;
import java.util.List;

import it.begear.indumenti_di_un_negozio.dao.DAOIndumenti_di_un_negozio;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_donna;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_uomo;
import it.begear.indumenti_di_un_negozio.service.Inserimento_donna;
import it.begear.indumenti_di_un_negozio.service.Inserimento_uomo;
import it.begear.indumenti_di_un_negozio.util.ScannerManager;



public class Array {
    private  static List<Abbigliamento>lista=new ArrayList<>();


	public static void save(Abbigliamento_donna vestito) {
		if (lista != null) {
			lista.add(vestito);
		}
	}
	public static void save(Abbigliamento_uomo vestito) {
		if (lista != null) {
		    lista.add(vestito);
		}
	}


	
	public static void stampa() {
		for(int i=0;i<lista.size();i++) {
			Abbigliamento p=lista.get(i);
			System.out.println(p);
		}
	}
	public static void elimina_elemento() {
		System.out.println("dimmi il nome dell'indumento da eliminare");
		String nome1;
		nome1=ScannerManager.scrivere_stringa();
		int i=0;
		Abbigliamento p=lista.get(i);
		while(i<lista.size()&&!(nome1.equals(p.getNome()))) {
			i++;
		}
		if(i<lista.size()) {
			lista.clear();
			
		}
		else System.out.println("indumento non presente ");
	}
	
	public static void ricerca() {
		System.out.println("dimmi il nome dell'indumento da ricercare");
		String nome3;
		nome3=ScannerManager.scrivere_stringa();
		int i=0;
		Abbigliamento p=lista.get(i);
		while(i<lista.size()&&!(nome3.equals(p.getNome()))) {
			i++;
		}
		if(i<lista.size()) {
			System.out.println(p);
			
		}
		else System.out.println("indumento non presente ");
	}
    public static void venduto() {
    	System.out.println("dimmi il nome dell'indumento da vendere");
		String nome4;
		nome4=ScannerManager.scrivere_stringa();
		int i=0;
		Abbigliamento p=lista.get(i);
		while(i<lista.size()&&!(nome4.equals(p.getNome()))) {
			i++;
		}
		if(i<lista.size()) {
			p.venduto();
			
		}
		else System.out.println("indumento non presente ");
	}
    public static void scontato() {
    	System.out.println("dimmi il nome dell'indumento su cui  fare lo sconto");
		String nome5;
		nome5=ScannerManager.scrivere_stringa();
		int i=0;
		Abbigliamento p=lista.get(i);
		while(i<lista.size()&&!(nome5.equals(p.getNome()))) {
			i++;
		}
		if(i<lista.size()) {
			p.scontato();
			
		}
		else System.out.println("indumento non presente ");
	}
    
    }


