package it.begear.indumenti_di_un_negozio.service;


import it.begear.indumenti_di_un_negozio.dao.DAOIndumenti_di_un_negozio;
import it.begear.indumenti_di_un_negozio.dao.DAOIndumenti_di_un_negozioImpl;
import it.begear.indumenti_di_un_negozio.exc.MiaException;
import it.begear.indumenti_di_un_negozio.exc.MiaException2;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_donna;
import it.begear.indumenti_di_un_negozio.util.ScannerManager;

public class Inserimento_donna {
	public static Abbigliamento_donna inserimento_donna() throws MiaException, MiaException2{
		Abbigliamento_donna vestito=new Abbigliamento_donna();
		System.out.println("cosa vuoi inserire?");
		vestito.setNome(ScannerManager.scrivere_stringa());
		System.out.println("quanti ne vuoi inserire?");
	    int totale=ScannerManager.scrivere_numero();
	    if(totale==0) {
	    	throw new MiaException();
	    }
		vestito.setTotale(totale);
		System.out.println("di che materiale � fatto?");
		vestito.setMateriale(ScannerManager.scrivere_stringa());
		System.out.println("prezzo?");
		int prezzo=ScannerManager.scrivere_numero();
		if(prezzo==0)throw new MiaException2();
		vestito.setPrezzo(prezzo);
		System.out.println("id donna?");
		int id_donna=ScannerManager.scrivere_numero();
		vestito.setId_donna(id_donna);
		System.out.println("� attraente?true-false");
		vestito.setAttraente(ScannerManager.scrivere_boolean());
		System.out.println("� corto?true-false");
		vestito.setCorto(ScannerManager.scrivere_boolean());
		return vestito;
	}
}
