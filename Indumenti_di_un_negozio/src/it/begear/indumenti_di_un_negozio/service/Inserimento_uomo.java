package it.begear.indumenti_di_un_negozio.service;


import it.begear.indumenti_di_un_negozio.dao.DAOIndumenti_di_un_negozio;
import it.begear.indumenti_di_un_negozio.dao.DAOIndumenti_di_un_negozioImpl;
import it.begear.indumenti_di_un_negozio.exc.MiaException;
import it.begear.indumenti_di_un_negozio.exc.MiaException2;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_donna;
import it.begear.indumenti_di_un_negozio.model.Abbigliamento_uomo;
import it.begear.indumenti_di_un_negozio.util.ScannerManager;

public class Inserimento_uomo {
	public static Abbigliamento_uomo inserimento_uomo() throws MiaException, MiaException2 {
		Abbigliamento_uomo vestito=new Abbigliamento_uomo();
		System.out.println("cosa vuoi inserire?");
		vestito.setNome(ScannerManager.scrivere_stringa());
		System.out.println("quanti ne vuoi inserire?");
		int totale=ScannerManager.scrivere_numero();
		if (totale==0){
	    	throw new MiaException();
	    }
		vestito.setTotale(totale);
		System.out.println("di che materiale � fatto?");
		vestito.setMateriale(ScannerManager.scrivere_stringa());
		System.out.println("prezzo?");
		int prezzo=ScannerManager.scrivere_numero();
		if (prezzo==0){
	    	throw new MiaException2();
	    }
		vestito.setPrezzo(prezzo);
		System.out.println("id uomo?");
		int id_uomo=ScannerManager.scrivere_numero();
		vestito.setId_uomo(id_uomo);
		System.out.println("� da cerimonia?true-false");
		vestito.setDa_cerimonia(ScannerManager.scrivere_boolean());
		System.out.println("� sportivo?true-false");
		vestito.setSportivo(ScannerManager.scrivere_boolean());
		return vestito;
	}
	
}
