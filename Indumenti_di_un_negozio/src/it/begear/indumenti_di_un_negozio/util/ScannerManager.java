package it.begear.indumenti_di_un_negozio.util;

import java.util.Scanner;

public class ScannerManager {
	public static int scrivere_numero() {
		int numero;
		Scanner Tastiera = new Scanner(System.in);
		try {
			numero = Tastiera.nextInt();
			return numero;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("hai sbagliato,riscrivi il numero");
			String flush=Tastiera.nextLine();
			return scrivere_numero();
		}
	}

	public static String scrivere_stringa() {
		String parola;
		Scanner Tastiera = new Scanner(System.in);
		try{
			parola = Tastiera.nextLine();
			return parola;
		} catch(Exception e) {
			System.out.println("hai sbagliato,riscrivi la parola");
			String flush=Tastiera.nextLine();
			return scrivere_stringa();
		}
	}

	public static boolean scrivere_boolean() {
		boolean affermazione;
		Scanner Tastiera = new Scanner(System.in);
		try{
			affermazione = Tastiera.nextBoolean();
			return affermazione;
		} catch(Exception e){
			System.out.println("hai sbagliato,riscrivi la parola");
			String flush=Tastiera.nextLine();
			return scrivere_boolean();
		}
	}
}
